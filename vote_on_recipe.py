#!/usr/bin/env python

"""
	Authors: Victor Trejo
	This scripts is called from the web application everytime an user vote on a recipe.
"""
import utilities as utl
import json
import cgi

# Reading data from the http request
form = cgi.FieldStorage()

users = utl.LoadUsers()


user = None

for u in users:
	if u['userId']==int(form['userId'].value):
		user = u
		break

user['ratings'].append({'recipeId': int(form['recipeId'].value), 'value':int(form['value'].value)})

usersFolderPath = 'data/users/'


jsonFileLocation = '{0}{1}.json'.format(\
	usersFolderPath,
	user['userId']
)

with open(jsonFileLocation, 'w') as jsonFile:
	json.dump(user, jsonFile)


# Printing the the result of the request as a json so it can be 
# used by the web application.
print "Content-type: text/json\n"
print json.dumps(user)