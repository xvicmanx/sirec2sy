#!/usr/bin/env python

"""
	Authors: Victor Trejo
	This script is used by the application demo to get all the recipes of the system
"""
import utilities as utl
import json

recipes = utl.LoadRecipes()
for recipe in recipes:
	recipe['image_url'] = utl.GetRecipeImageUrl(recipe['title'])
# Printing the the result of the request as a json so it can be 
# used by the web application.
print "Content-type: text/json\n"
print json.dumps(recipes)