"""
Author: Victor Trejo
Description: This scripts is to create the fake users.
"""

from nltk.corpus import names
import random
import json
import utilities as utl

def generateUserRatings():
	"""
	Generates fake ratings on recipes 
	Returns: a list of fake ratings
	"""
	ratings = [] 
	numberOfRatings = random.randint(5, 10)

	recipesIds = [i for i in range(1, 260)]
	random.shuffle(recipesIds)


	for i in range(numberOfRatings):	
		# 1 means upvote, 0 means down vote
		ratingValue = random.randint(0,1)
		rating = {\
			'recipeId': recipesIds[i],
			'value': ratingValue
		}
		ratings.append(rating)
	return ratings


femaleNames = names.words('female.txt')
maleNames = names.words('male.txt')
allNames = list(set(femaleNames).union(maleNames))
random.shuffle(allNames)

usersFolderPath = 'data/users/'

utl.cleanFolder(usersFolderPath)
# Generating 30 users
userCount = 0
ratingsCount = 0
for name in allNames[0:10]:
	userCount+=1
	jsonFileLocation = '{0}{1}.json'.format(\
		usersFolderPath,
		userCount
	)
	user = {\
		'userId': userCount,
		'name':	name
	}

	user['ratings'] = generateUserRatings()
	
	with open(jsonFileLocation, 'w') as jsonFile:
		json.dump(user, jsonFile)