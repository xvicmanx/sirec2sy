var recipesApplication = angular.module('recipesApp',['ngRoute', 'ngCookies']);






recipesApplication.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/dashboard', {
        templateUrl: 'front_end/templates/dashboard.html',
        controller: 'BestRatedRecipesController'
      }).
      when('/recipe/:recipeId/:recipeTitle', {
        templateUrl: 'front_end/templates/recipe.html',
        controller: 'RecipeDetailController'
      }).
       when('/searchResult/:searchQuery', {
        templateUrl: 'front_end/templates/search_results.html',
        controller: 'SearchResultsController'
      }).
       when('/users', {
        templateUrl: 'front_end/templates/users.html',
        controller: 'UsersController'
      }).
       when('/recipes', {
        templateUrl: 'front_end/templates/all_recipes.html',
        controller: 'RecipesController'
      }).
        when('/user/:userId/:userName', {
        templateUrl: 'front_end/templates/user_profile.html',
        controller: 'UserController'
      }).
      otherwise({
        redirectTo: '/dashboard'
      });
  }]);








recipesApplication.controller('BestRatedRecipesController', function($scope, $http, $location) {
  
  $scope.getRange = function(times) {
	    return new Array(times);   
  }
  
  $http({method: 'POST', url: 'best_ranked_recipes.py'}).success(function(data)
  {
	$scope.recipes = data;
   });


});


recipesApplication.controller('RecipesController', function($scope, $http, $location) {
  
  $scope.getRange = function(times) {
      return new Array(times);   
  }
  
  $http({method: 'POST', url: 'get_all_recipes.py'}).success(function(data)
  {
    $scope.recipes = data;
   });

});


recipesApplication.controller('UserDashboardController', function($scope, $http, $location) {
  
  
  

    $http.get('get_user_recommended_recipes.py?userId='+   $scope.user.userId).success(function(data)
    {
       $scope.recipes = data;
    
    });

});


recipesApplication.controller('RecipeDetailController', function($scope, $http, $location, $routeParams) {

  
  $http.get('recipe_info.py?recipeId='+  $routeParams.recipeId).success(function(data)
  {
	   $scope.recipe = data; 
	
   });
});



recipesApplication.controller('PageController', function($scope, $http, $location, $routeParams, $route) {
  
  $scope.searchRecipe = function()
  {
    query = $scope.searchQuery
    $scope.searchQuery = ''
    $location.path( "/searchResult/"  + query);
     
  }

   $scope.goToProfile = function(recipeId, title)
  {
     $location.path( "/recipe/"  + recipeId + "/" + title.replace(/\s/g, "-"));
  }

    $scope.user = {userId: -1, name:'', recipesVoted:[]}
  
    $scope.isUserLoggedIn = function()
    {
      return $scope.user.userId!=-1      
    }

    
    
    $scope.updateUser = function(user)
    {
      $scope.user = user      
    }

    $scope.hasAnyVote = function(){
      return $scope.user.recipesVoted.length > 0;   
    }

    $scope.hasUserVoted = function(recipeId)
    {
      if ($scope.user.userId==-1)
        return false;

        for (var i in $scope.user.recipesVoted) {
             recipe = $scope.user.recipesVoted[i]
             if (recipe.recipeId == recipeId)
                return true;
        }

      return false;
    }

    $scope.hasVoteValue = function(recipeId, value)
    {
        if ($scope.user.userId==-1)
        return false;

        for (var i in $scope.user.recipesVoted) {
             recipe = $scope.user.recipesVoted[i]
             if (recipe.recipeId == recipeId)
                return recipe.value==value;
        }

        return false;
    }

    $scope.voteOnRecipe = function(recipeId, vote)
    {
      
      $http.get('vote_on_recipe.py?userId='+  $scope.user.userId + "&recipeId=" + recipeId + "&value="+ vote).success(function(data)
      {
          $scope.user.recipesVoted.push({recipeId:recipeId, value:vote});
          $route.reload();

      });
    }
});


recipesApplication.controller('SearchResultsController', function($scope, $http, $location, $routeParams) {
  
  
  $http.get('get_recipes_for_query.py?query='+  $routeParams.searchQuery).success(function(data)
  {
    $scope.recipes = data;  
  
   });
  
});


recipesApplication.controller('UsersController', function($scope, $http, $location, $routeParams, $cookies) {
  
  
  $http.get('get_users.py').success(function(data)
  {
    $scope.users = data;  
   });

   $scope.createNewUser = function(userToCreate){
      $http.get('create_new_user.py?userName='+  userToCreate).success(function(data)
      {
          user = data
          $location.path( "/user/"+user.userId+"/"+user.name);
       });
   }

   $scope.login= function(userId, userName, recipesVoted)
   {
      user = {userId:userId, name:userName, recipesVoted:recipesVoted};
      $cookies.put('user', user);
      $scope.updateUser(user);
      $location.path( "/dashboard/");
   }

});


recipesApplication.controller('UserController', function($scope, $http, $location, $routeParams) {
  
  $http.get('get_user_info.py?userId='+  $routeParams.userId).success(function(data)
  {
     $scope.user = data; 
   });



});