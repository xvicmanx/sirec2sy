"""
Author: Victor Trejo
Description: This script takes the recipes raw/xml and convert them to JSON format.
"""
import xml.etree.ElementTree as ET
import os
import json
import utilities as utl




def parseRecipesXMLFile(fileLocation, recipeId):
	"""
	Parses a recipe XML file specified by the file location.
	Parameters:
				fileLocation -> location of the file to be parsed
				recipeId     -> id to be assigned to the recipe
	Returns: Returns a dictionary object that represents the parsed
			 recipe.
	"""
	recipeName = os.path.basename(fileLocation).split('.')[0]
	title =  recipeName.replace("-", " ")

	result = {\
		'recipeId': recipeId,
		'title': title,
		'ingredients':[],
		'instructions':[],
		'preprocessed_ingredients':[],
		'preprocessed_title':[]
	}

	tree = ET.parse(fileLocation)
	recipeElement = tree.getroot()
	addingInstructions = False
	for lineElement in recipeElement:
		isAnIngredient = 'create_ing' in lineElement.find('annotation').text
		# This is to mark where to start processing instructions
		if not addingInstructions and not isAnIngredient:
			addingInstructions = True

		originaltext =  lineElement.find('originaltext').text
		recipeSection =  'ingredients' if isAnIngredient\
						  and not addingInstructions else 'instructions'
		result[recipeSection].append(originaltext)
	result['instructions'] = list(set(result['instructions']))
	result['preprocessed_ingredients'] = utl.PreprocessRecipeIngredients(result['ingredients'], result['instructions']  )
	result['preprocessed_title'] = utl.PreprocessRecipeIngredients([result['title']], result['instructions'] )
	print result['preprocessed_title']
	return result




rawRecipesFolderPath = 'data/annotated_recipes/'
processedRecipesFolderPath = 'data/json_format_recipes/'
recipeCount = 0
# Going through all the recipes xml files, parsing them and saving
# the result in to json file whose name is the {RECIPE_ID}.json
for (dirPath, dirNames, fileNames) in os.walk(rawRecipesFolderPath):
	for fileName in fileNames:
		
		fileLocation = '{0}{1}'.format(\
			rawRecipesFolderPath,
			fileName
		)
		
		if not 'svn-base' in fileLocation and '.xml' in fileLocation: 
			recipeCount+=1
			# Parsing recipe
			print("Parsing recipe {}".format(recipeCount))
			recipe = parseRecipesXMLFile(\
				fileLocation,
				recipeCount
			)
		
			jsonFileLocation = '{0}{1}.json'.format(\
				processedRecipesFolderPath,
				recipeCount
			)

			with open(jsonFileLocation, 'w') as jsonFile:
				json.dump(recipe, jsonFile)