#!/usr/bin/env python
 
"""
	Authors: Victor Trejo
	This scripts is the starting point for the recipes demo web application.
	It reads the index html file and prints its content.
"""
print "Content-type: text/html\n"
html = open('front_end/index.html').read()
print html