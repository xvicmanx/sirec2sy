"""
Author: Victor Trejo
Description: This script is to get the samples images urls for the recipes.
"""
import utilities as utl
import json
import time

# The key need to be provided when the script is run 
# by replacing this variable.
key = '{KEY}'

imagesUrls = {}
for recipe in utl.LoadRecipes():
	title = recipe['title']
	time.sleep(1)
	imagesUrls[title] = utl.GetRecipeSampleImage(title, key)
	print "{0} {1}".format(title, imagesUrls[title])

#Location to store all those recipes image urls.
fileLocation = "data/recipes_image_urls_map.json"
with open(fileLocation, "w") as jsonFile:    
		    json.dump(imagesUrls, jsonFile)