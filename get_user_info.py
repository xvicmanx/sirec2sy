#!/usr/bin/env python

"""
	Authors: Victor Trejo
	This script is used by the application demo to get information of an user.
"""
import utilities as utl
import json
import cgi

# Reading data from the http request
form = cgi.FieldStorage()

# Getting all the recipes
recipes = {recipe['recipeId']:recipe for recipe in utl.LoadRecipes()}
userId = int(form['userId'].value)

user = None
for u in utl.LoadUsers():
	if u['userId'] == userId:
		user = u
		likedRecipes = []
		dislikedRecipes = []
		for rating in user['ratings']:
			if rating['value']==1:
				likedRecipes.append(recipes[rating['recipeId']])
			else:
				dislikedRecipes.append(recipes[rating['recipeId']])
		user['likedRecipes'] = likedRecipes
		user['dislikedRecipes'] = dislikedRecipes
		break


# Printing the the result of the request as a json so it can be 
# used by the web application.
print "Content-type: text/json\n"
print json.dumps(user)