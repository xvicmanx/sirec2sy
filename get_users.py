#!/usr/bin/env python

"""
	Authors: Victor Trejo
	This script is used by the application demo to get all the users of the system.
"""
import utilities as utl
import json

# Printing the the result of the request as a json so it can be 
# used by the web application.
print "Content-type: text/json\n"
print json.dumps(utl.LoadUsers())