#!/usr/bin/env python

"""
	Authors: Victor Trejo
	This script is used by the application demo to get the recommended recipes to an user 
	based on his interests.
"""
import utilities as utl
import json
import cgi

# Reading data from the http request
form = cgi.FieldStorage()

# Getting all the recipes
recipes = {recipe['recipeId']:recipe for recipe in utl.LoadRecipes()}
users = utl.LoadUsers()
user = None

for u in users:
	if u['userId'] == int(form['userId'].value):
		user = u
		break

positiveVotedRecipesIds = []
negativeVotedRecipesIds = []
for vote in user['ratings']:
	if vote['value'] == 1:
		positiveVotedRecipesIds.append(vote['recipeId'])
	else:
		negativeVotedRecipesIds.append(vote['recipeId'])


toReturnRecipes = []

if len(positiveVotedRecipesIds) > 0:
	profile = []
	for recipeId in positiveVotedRecipesIds:
		profile.extend(recipes[recipeId]['preprocessed_ingredients'])

	# Number of recipes to be retrieved.
	howManyRecipes = 20
	votes = utl.getRecipesVotes()
	# Getting recipes similar to the profile
	processedRecipes = utl.GetSimilarInstances(profile, 260)
	for otherRecipe in processedRecipes:
		otherRecipe['image_url'] =  utl.GetRecipeImageUrl(otherRecipe['title'])
		otherRecipe['votes'] = [votes[otherRecipe['recipeId']].count(1),  votes[otherRecipe['recipeId']].count(0)]

	
	count = 0
	votedRecipes = positiveVotedRecipesIds + negativeVotedRecipesIds
	for recipe in processedRecipes:
		if not recipe['recipeId'] in votedRecipes:
			count+=1
			toReturnRecipes.append(recipe)  
		if count == howManyRecipes: break

	# Printing the the result of the request as a json so it can be 
	# used by the web application.
	print "Content-type: text/json\n"
	print json.dumps(toReturnRecipes)

elif len(negativeVotedRecipesIds) > 0:
	profile = []
	for recipeId in negativeVotedRecipesIds:
		profile.extend(recipes[recipeId]['preprocessed_ingredients'])

	# Number of recipes to be retrieved.
	howManyRecipes = 20
	votes = utl.getRecipesVotes()
	# Getting recipes similar to the profile
	processedRecipes = utl.GetSimilarInstances(profile, 260)
	for otherRecipe in reversed(processedRecipes):
		otherRecipe['image_url'] =  utl.GetRecipeImageUrl(otherRecipe['title'])
		otherRecipe['votes'] = [votes[otherRecipe['recipeId']].count(1),  votes[otherRecipe['recipeId']].count(0)]

	
	count = 0
	votedRecipes = positiveVotedRecipesIds + negativeVotedRecipesIds
	for recipe in processedRecipes:
		if not recipe['recipeId'] in votedRecipes:
			count+=1
			toReturnRecipes.append(recipe)  
		if count == howManyRecipes: break

	# Printing the the result of the request as a json so it can be 
	# used by the web application.
	print "Content-type: text/json\n"
	print json.dumps(toReturnRecipes)