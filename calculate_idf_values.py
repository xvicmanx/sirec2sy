"""
Author: Victor Trejo
Description: This documents calculates the Inverse Document Frequency of all the terms/words 
			 and store this values in a file so they can be used in the future and do not 
			 have to be calculated every time.
"""

import json
import utilities as utl

allWords = []
allDocuments = []

for recipe in utl.LoadRecipes():
	ingredients = recipe['preprocessed_ingredients']
	ingredients.extend(recipe['preprocessed_title'])
	allDocuments.append(ingredients)
	allWords.extend(ingredients)

allWords = set(allWords)
idfValues = utl.InverseDocumentFrequency(allWords, allDocuments)

idfValuesFileLocation = "data/idf_values.json"
with open(idfValuesFileLocation, "w") as jsonFile:    
		    json.dump(idfValues, jsonFile)




