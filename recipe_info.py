#!/usr/bin/env python

"""
Authors: Victor Trejo
This script is used by the application demo to get information of a recipe by its id.
"""
import json
import cgi
import utilities as utl


# Reading data from the http request
form = cgi.FieldStorage()
# loading recipe by id
recipe = utl.LoadRecipeById(form['recipeId'].value)
# Getting image of the recipe
recipe['image_url'] = utl.GetRecipeImageUrl(recipe['title'])

# Number of recipes to be retrieved.
howManyRecipes = 10

# Getting recipes that are simmilar to the current recipe
ingredients = recipe['preprocessed_ingredients']
ingredients.extend(recipe['preprocessed_title'])
processedRecipes = utl.GetSimilarInstances(\
	ingredients,
	 howManyRecipes,
	 int(form['recipeId'].value)
)

votes = utl.getRecipesVotes()

# Getting images for each similar recipe to the current recipe
for otherRecipe in processedRecipes:
	otherRecipe['image_url'] =  utl.GetRecipeImageUrl(otherRecipe['title'])
	otherRecipe['votes'] = [votes[otherRecipe['recipeId']].count(1),  votes[otherRecipe['recipeId']].count(0)]
recipe['similarRecipes'] = processedRecipes
recipe['votes'] = [votes[recipe['recipeId']].count(1),  votes[recipe['recipeId']].count(0)]

# Printing the the result of the request as a json so it can be 
# used by the web application.
print "Content-type: text/json\n"
print json.dumps(recipe)
