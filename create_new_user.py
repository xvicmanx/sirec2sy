#!/usr/bin/env python

"""
	Authors: Victor Trejo
	This script is used by the application demo to create a new user.

"""
import utilities as utl
import json
import cgi

# Reading data from the http request
form = cgi.FieldStorage()

user = {
	'name':form['userName'].value,
	'userId': len(utl.LoadUsers()) + 1,
	'ratings':[],
	'likedRecipes': [],
	'dislikedRecipes': []
}


usersFolderPath = 'data/users/'


jsonFileLocation = '{0}{1}.json'.format(\
	usersFolderPath,
	user['userId']
)

with open(jsonFileLocation, 'w') as jsonFile:
	json.dump(user, jsonFile)


# Printing the the result of the request as a json so it can be 
# used by the web application.
print "Content-type: text/json\n"
print json.dumps(user)