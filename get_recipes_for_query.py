#!/usr/bin/env python

"""
	Authors: Victor Trejo
	This script is used by the application demo to get information of 
	recipes that are similar to given query
"""
import utilities as utl
import json
import cgi

# Reading data from the http request
form = cgi.FieldStorage()

# Preprocessing the query so it can be compared to the recipes.
queryPreprocessed = utl.PreprocessRecipeIngredients(\
	form['query'].value.split()
)

# Number of recipes to be retrieved.
howManyRecipes = 20


votes = utl.getRecipesVotes()
# Getting recipes similar to the query
processedRecipes = utl.GetSimilarInstances(queryPreprocessed, howManyRecipes)
for otherRecipe in processedRecipes:
	otherRecipe['image_url'] =  utl.GetRecipeImageUrl(otherRecipe['title'])
	otherRecipe['votes'] = [votes[otherRecipe['recipeId']].count(1),  votes[otherRecipe['recipeId']].count(0)]

# Printing the the result of the request as a json so it can be 
# used by the web application.
print "Content-type: text/json\n"
print json.dumps(processedRecipes)