"""
Author: Victor Trejo
Description: This script contains some of the core functionalities used in the project.
"""

import math
import os
import json
import nltk
import urllib
from nltk.corpus import stopwords
from nltk.corpus import wordnet as wn
from nltk import word_tokenize
from nltk.wsd import lesk
import networkx as nx

def InverseDocumentFrequency(words, documents):
	"""
	Calculates the inverse document frequency of a set of words.
	Parameters:
				words 	  -> words to find the inverse document frequency
				documents -> set of documents.
	Returns: a dictionary where each word is a key and the value is 
			 its inverse document frequency 
	"""
	wordsIDF = {word:0.0 for word in words}
	numberOfDocuments = len(documents)
	for word in words:
		documentsContainingWord = 0
		for document in documents:
			if word in document:
				documentsContainingWord+=1
		wordsIDF[word] = math.log(float(numberOfDocuments)/documentsContainingWord)
	return wordsIDF

def TermFrequency(words, document):
	"""
	Calculates the term frequency of a set of words.
	Parameters:
				words 	  -> words to find the inverse document frequency
				document -> document to look inside.
	Returns: a dictionary where each word is a key and the value is 
			 its frequency in the document.
	"""
	wordsFrequency = {word:0 for word in words}
	documentCopy = [word for word in document]

	for word in words:
		if word in documentCopy:
			wordsFrequency[word]+=1
			# wordsFrequency[word]=1
			documentCopy.remove(word)
	return wordsFrequency

def TermFrequencyInverseDocumentFrequency(\
	tfDocumentValues,
	idfValues
):
	"""
	Calculates the tf-idf values for all the terms in a document
	Parameters: 
				tfDocumentValues -> term frequeny values
				idfValues 		 -> inverse document frequeny values
	Returns: the tf-idf values.
	"""
	return {\
		word:tfDocumentValues[word]*idfValues[word] 
		for word in idfValues.keys()
	}

def CosineSimilarity(
	firstVector,
	secondVector,
	allWords
):
	"""
	Calculates the cosine similarity between two document vectors.
	Parameters:
			   firstVector  -> first document vector
			   secondVector -> second document vector
			   allWords     -> set of all words
	Returns: the cosine similarity value			
	"""
	numerator = sum(\
		firstVector[word]*secondVector[word]
		for word in allWords
	)

	firstSquaredSum = sum(\
		firstVector[word]**2
		for word in allWords
	)

	secondSquaredSum = sum(\
		secondVector[word]**2
		for word in allWords
	)

	denominator = math.sqrt(firstSquaredSum) * math.sqrt(secondSquaredSum)
	return numerator/denominator if denominator > 0 else 0.0



def LoadUsers(usersFolderPath='data/users/'):
	"""
	Reads the users json file and returns them
	Parameters: 
				usersFolderPath -> folder path where the users files are stored.
	Returns: a list with the read users
	"""
	users = []
	for (dirPath, dirNames, fileNames) in os.walk(usersFolderPath):
		for fileName in fileNames:
			fileLocation = "{0}{1}".format(\
				usersFolderPath,
				fileName
			)
			with open(fileLocation) as jsonFile:
				users.append(json.load(jsonFile))
	return users

def LoadRecipeById(recipeId,recipesFolderPath='data/json_format_recipes/'):
	"""
	Read the recipes json file and returns id
	Parameters: 
				recipeId -> id of the recipe
				recipesFolderPath -> folder path where the recipes files are stored.
	Returns: the read recipe
	"""
	recipe = None
	fileLocation = "{0}{1}".format(\
		recipesFolderPath,
		"{0}.json".format(recipeId)
	)
	with open(fileLocation) as jsonFile:
		recipe = json.load(jsonFile)
	return recipe


def LoadRecipes(recipesFolderPath='data/json_format_recipes/'):
	"""
	Reads the recipes json file and returns them
	Parameters: 
				recipesFolderPath -> folder path where the recipes files are stored.
	Returns: a list with the read recipes
	"""
	recipes = []
	for (dirPath, dirNames, fileNames) in os.walk(recipesFolderPath):
		for fileName in fileNames:
			fileLocation = "{0}{1}".format(\
				recipesFolderPath,
				fileName
			)
			with open(fileLocation) as jsonFile:
				recipes.append(json.load(jsonFile))
	return recipes


lemmatizer = None
cachedLemmatizedResults = None
def lemmatizeWord(word):
	"""
	Applies word net lemmatization to a given word.
	Parameters:
				word -> word to be lemmatized.
	Returns: the word after being lemmatized.
	"""
	jsonFileLocation = 'data/lemmatized_outputs.json'
	
	# In order to make the process faster:
	# We are storing the output/results of the lemmatization
	# of the words.
	global cachedLemmatizedResults
	# If the cached results have not been loaded
	if cachedLemmatizedResults is None:
		# It is read from the file
		result = None
		with open(jsonFileLocation, 'rw') as jsonFile:
			try: result = json.load(jsonFile)
			except Exception, e: result = None
			finally: jsonFile.close()
		cachedLemmatizedResults = {} if result is None else result

	# If we do not have the result of the lemmatization of a word
	# we store it in the cache file
	if word  not in cachedLemmatizedResults:
		global lemmatizer
		if lemmatizer is None: 
			lemmatizer = nltk.stem.WordNetLemmatizer()
		cachedLemmatizedResults[word] = lemmatizer.lemmatize(word)

		
		with open(jsonFileLocation, 'w') as jsonFile: 
			json.dump(cachedLemmatizedResults, jsonFile)

	# returning lemmatization result.
	return cachedLemmatizedResults[word]


def getFirstMatchingContext(sentences, targetWord):
	"""
	Given a list of sentences, finds the first sentence where
	the word is used.
	Parameters:
				sentences  -> sentence to look for the word.
				targetWord -> word to search.
	Returns:	The first matching sentence.
	"""
	for sentence in sentences:
		tokens = nltk.wordpunct_tokenize(sentence.lower())
		tokens = [lemmatizeWord(word) for word in tokens if word.isalpha()]
		if targetWord in tokens:
			return tokens
	return []




stopWords = None
def PreprocessRecipeIngredients(ingredients, instructions = []):
	"""
	Preprocesses the ingredients list of a recipe
	Parameters:
				ingredients -> ingredients list
	Returns: a list of words that represent the preprocessed ingredients list.
	"""
	result = []


	# Some the preprocessing steps that were applied were:
	# tokenization, stopwords elimination, puntuaction elimination,
	# lowercasing the of the tokens and lemmatization.
	global stopWords
	if stopWords is None: stopWords = stopwords.words('english')

	types = [wn.ADJ, wn.ADV, wn.NOUN]
	
	
	notIncluded = lambda ing: (isAMeasure(ing) and not isRelatedToFood(ing)) or 'cup'==ing or ing=='taste'
	

	for ingredientLine in ingredients:

		tokens = nltk.wordpunct_tokenize(ingredientLine.lower())
		filteredTokens = [word for word in tokens if word not in stopWords]
		processedTokens = [lemmatizeWord(word) for word in filteredTokens if word.isalpha()]
		processedTokens = [word for word in processedTokens if not notIncluded(word)]

		synsets = []
		for word in processedTokens:
			if word.isalpha():
				context = tokens
				if len(instructions) > 0:
					context = getFirstMatchingContext(instructions, word)

				synset = bestSynset(context, word, types)
				
				synsets.append(synset.name().split(".")[0] if not synset is None else word)
		
		result.extend([synset for synset in synsets if len(synset) > 0])
	return result


# To fix some  desambiguation issuess
synsetsRestriction = {\
		'oil': ['vegetable_oil.n.01'],
		'cheese':['cheese.n.01'],
		'salt':['salt.n.01', 'salt.n.02', 'salt.n.04'],
		'ounce':['ounce.n.01', 'ounce.n.02'],
		'chicken':['chicken.n.01', 'chicken.n.02', 'chicken.n.04'],
		'sausage':['sausage.n.01'],
		'beef':['beef.n.01', 'beef.n.02'],
		'pie': ['pie.n.01'],
		'applesauce':['applesauce.n.01'],
		'ball': ['ball.n.01'],
		'bell': ['bell.n.01'],
		'brown': ['brown.s.01'],
		'cake': ['cake.n.03'],
		'can':['can.n.02'],
		'chocolate':['cocoa.n.01'],
		'water':['water.n.01'],
		'sugar':['sugar.n.01'],
		'stock': ['stock.n.01'],
		'sprig':['branchlet.n.01'],
		'spice':['spice.n.01', 'spice.n.02'],
		'soda':['sodium_carbonate.n.01'],
		'roll': ['roll.n.11'],
		'paprika': ['sweet_pepper.n.01'],
		'medium': ['medium.n.01'],
		'meat':['meat.n.01'],
		'leg':['leg.n.02'],
		'ground':['ground.n.09'],
		'grain':['grain.n.10'],
		'ginger': ['ginger.n.03'],
		'fruit':['fruit.n.01'],
		'flake':['bit.n.02'],
		'egg':['egg.n.01'],
		'drop':['drop.n.06'],
		'corn':['corn.n.01'],
		'fish':['fish.n.01']
}
def bestSynset(context, word, positions):
	"""
	Using Lesk algorithm finds the best synset of word given its
	context.
	Parameters:	
				context   -> context of the word.
				word      -> word to find the best synset.
				positions -> restriction of of the types of synsets to look up. 
	Returns: the best synset found by the algorithm.
	"""
	global synsetsRestriction
	synsets = None
	if word in synsetsRestriction.keys():
		synsets = [syn for syn in wn.synsets(word) if syn.name() in synsetsRestriction[word]]

	synset = None
	for pos in positions:
		synset = lesk(context, word, pos=pos, synsets = synsets)
		if not synset is None: break
	return synset

def LoadIDFValues(fileLocation = 'data/idf_values.json'):
	"""
	Reads the preprocessed idf values from its file
	Parameters: 
				fileLocation -> location where the values are stored
	Returns: the read values
	"""
	result = None
	with open(fileLocation) as jsonFile:
		result = json.load(jsonFile)
	return result


def GetRecipeSampleImage(recipeTitle, key):
	"""
	Gets an image for a recipe based on its title
	Parameters:
				recipeTitle -> recipe's title
				key         -> api's key
	Returns: the extracte image url
	"""
	query = urllib.quote(recipeTitle.encode('utf8'))

	apiUrl = "http://food2fork.com/api/search?key={0}&q={1}"
	response = urllib.urlopen(apiUrl.format(key, query))
	data = json.loads(response.read())
	if 'recipes' in data.keys():
		if len(data['recipes']) > 0:
			if 'image_url' in data['recipes'][0]:
				return data['recipes'][0]['image_url']
	return ""


imagesUrls = None
def GetRecipeImageUrl(title, fileLocation = 'data/recipes_image_urls_map.json'):
	"""
	Reads the preprocessed idf values from its file
	Parameters: 
				title 		 -> recipe's title 
				fileLocation -> location where the values are stored
	Returns: the read values
	"""
	global imagesUrls
	if imagesUrls is None:
		with open(fileLocation) as jsonFile:
			imagesUrls = json.load(jsonFile)


	return imagesUrls[title] if imagesUrls[title]!="" else "http://static.food2fork.com/noimage2f00.recipeimage.gif"


def GetSimilarInstances(query, total=10, notId = float("inf")):
	"""
	Gets all the recipes that are similar to a given query.
	Parameters:
				query -> query to find similar recipes
				total -> total of similar recipes to return.
				notId -> id of the recipe that should not be included in the similar recipes.
	Returns:  a list with the similar recipes to the query.
	"""
	idfValues = LoadIDFValues()
	allWords = idfValues.keys()

	queryTfIdf = TermFrequencyInverseDocumentFrequency(\
		TermFrequency(allWords, query),
		idfValues
	)

	recipesSimilarity = []

	for recipe in LoadRecipes():
		
		if recipe['recipeId'] == notId: continue

		ingredients = recipe['preprocessed_ingredients']
		ingredients.extend(recipe['preprocessed_title'])

		documentTfIdf = TermFrequencyInverseDocumentFrequency(\
			TermFrequency(allWords, ingredients),
			idfValues
		)

		similarity = CosineSimilarity(\
			documentTfIdf,
			queryTfIdf,
			allWords
		)

		
		recipesSimilarity.append((recipe, similarity))
	topRecipes = sorted(recipesSimilarity, key=lambda x: x[1], reverse=True)[0:total]
	return [recipe for recipe, value in topRecipes]


def getRecipesVotes():
	"""
	Gets the votes that have been made in all the recipes.
	Returns: a dictionary where the keys are the recipes ids and the values are a list of values
	"""
	# initializing the structure that will store the recipes votes
	votes = {recipeId:[] for recipeId in range(1, 261)}
	# Storing the votes that the users have given to each recipe
	for user in LoadUsers():
		for vote in user['ratings']:
			votes[vote['recipeId']].append(vote['value'])
	return votes

def calculateItemScore(votes):
	"""
	Calculates the score of the recipe item based on the number of
	up-votes and down-votes.
	Parameters:
				votes: a list of votes on the item
	"""
	# These values are used a smooth constant to 
	# penalize the items that have a few items.
	# An item should have a big number of upvotes over downvotes
	# to increase their score.
	fakeUpVotes, fakeDownVotes = 5, 5
	totalUpvotes = fakeUpVotes + votes.count(1)
	totalDownvotes = fakeDownVotes + votes.count(0)
	# The score is the rating over the total of upvotes over the total of votes.
	return totalUpvotes / float(totalUpvotes + totalDownvotes)



foodCategories = (\
	"fish",
	"beef",
	"meat",
	"beverage",
	"berry",
	"fruit",
	"green",
	"condiment",
	"spice",
	"vegetable",
	"herb",
	"sweet",
	"grain",
	"starch",
	"dairy",
	"food", 
	"beer"
)
def isRelatedToFood(word):
	"""
	Checks whether a given word is related to food or not.
	This is done by going through all its synsets hypernyms.
	If one of the hypernyms contains a word from  foodCategories
	Then it is related.
	Parameters:
				word -> target word to see if it is related to food.
	Returns:  True if it is related to food, False otherwise.
	"""
	global foodCategories
	for s in wn.synsets(word):
		hypernymsGraph = goThroughtAllHypernyms(s)
		for node in hypernymsGraph.nodes():
			for cat in foodCategories:
				if cat in str(node): return True	
	return False

def isAMeasure(word):
	"""
	Checks whether a given word is a measure word (cup, once, teaspoon, etc.)
	Parameters:
				word -> target word to see if it is a measure.
	Returns:  True if it is a measure, False otherwise.
	"""
	for s in wn.synsets(word):
		hypernymsGraph = goThroughtAllHypernyms(s)
		for node in hypernymsGraph.nodes():
			if 'measure' in str(node): return True	
	return False



def goThroughtAllHypernyms(synset):
	"""
	Creates a graph with all the hypernyms parents herarchy of a synset
	Parameters:
				synset -> synset to create the graph with its hypernyms parents.
	Returns: the created graph.
	"""
	seenSynsets = set()
	graph = nx.DiGraph()

	def moveRecursively(synset):
		if not synset in seenSynsets:
			seenSynsets.add(synset)
			graph.add_node(synset.name)

	 		for otherSynset in synset.hypernyms():
				graph.add_node(otherSynset.name)
				graph.add_edge(\
					synset.name,
					otherSynset.name
				)

				moveRecursively(otherSynset)

	moveRecursively(synset)
	return graph

def cleanFolder(folderPath):
	"""
	Cleans the folder specified by the path.
	Parameters:
				folderPath -> path of the folder to be cleaned up.
	"""
	for root, dirs, files in os.walk(folderPath):
		for fileName in files:
			os.remove("{0}{1}".format(root, fileName))