#!/usr/bin/env python

"""
	Authors: Victor Trejo
	This script is used by the application demo to get information of the best ranked recipes.
	This is initally used when there is no information of a user. (To solve the cold start problem)
"""
import utilities as utl
import json


# Getting all the recipes
recipes = {recipe['recipeId']:recipe for recipe in utl.LoadRecipes()}
# initializing the structure that will store the recipes ratings
ratings = utl.getRecipesVotes()

# Calculating the damped average of the ratings of each recipe.
recipesRatings = [\
(recipeId, utl.calculateItemScore(values))
 for recipeId, values in ratings.items()]

# Sorting the recipes by average rate in decreasing order.
recipesRatings = sorted(recipesRatings, key=lambda x: x[1], reverse=True)

# Getting the top 30 of these.
topRecipes = []
for recipeId, value in recipesRatings[0:30]:
	recipes[recipeId]['image_url'] = utl.GetRecipeImageUrl(recipes[recipeId]['title'])
	# number of uptvotes, number of downvotes
	recipes[recipeId]['votes'] = [ratings[recipeId].count(1), ratings[recipeId].count(0)]
	topRecipes.append(recipes[recipeId])


# Printing the the result of the request as a json so it can be 
# used by the web application.
print "Content-type: text/json\n"
print json.dumps(topRecipes)