"""
Author: Victor Trejo
Description: To evaluate the models.
"""
import utilities as utl

def getRecipesByMatchingTitle(query, total):
	query = set(query)
	recipes = utl.LoadRecipes()

	matches = {recipe['recipeId']:0 for recipe in recipes}
	mappedRecipes = {recipe['recipeId']:recipe for recipe in recipes}
	for recipe in  recipes:
		# print recipe['preprocessed_title']
		title = set(recipe['preprocessed_title'])
		common = query.intersection(title)
		matches[recipe['recipeId']] = len(common)


	sortedValues = sorted([(k,  v) for k,v in matches.items()], key=lambda x: x[1], reverse=True)
	result = []
	for k, v in sortedValues[:total]:
		result.append(mappedRecipes[k]['title'])
	return result

def getRecipesBySimilarity(query, total):
	queryPreprocessed = utl.PreprocessRecipeIngredients(\
		query
	)

	processedRecipes = utl.GetSimilarInstances(queryPreprocessed, total)
	result = []
	for recipe in processedRecipes:
		result.append(recipe['title'])
	return result



def calculateAccuracy(matches, groundTruth):
	matched = len(set(matches).intersection(set(groundTruth)))
	return 100*float(matched)/len(groundTruth)



def firstExperiment():
	# Experiment with one word: Comparing our algorithm against the baseline

	# Query
	query = ["avocado"]

	# Ground Truth
	groundTruth = [
	"Avocado Tzatziki",
	"Citrus Infused Guacamole",
	"California Chicken",
	"Awesome Turkey Sandwich",
	"Fab Summer Blackened Chicken Salad"
	]

	# Baseline: Matching recipes based on title.
	matchedByTitle =  getRecipesByMatchingTitle(query, len(groundTruth))
	# Matching by similarity
	matchedBySimilarity = getRecipesBySimilarity(query, len(groundTruth))

	# Evaluationg each method:
	print "Baseline Accuracy"
	print calculateAccuracy(matchedByTitle, groundTruth)
	print "Similarity Algorithm accuracy"
	print calculateAccuracy(matchedBySimilarity, groundTruth)



def secondExperiment():
	# Experiment with one word: Comparing our algorithm against the baseline

	# Query
	query = ["Apple", "Pie"]

	# Ground Truth
	groundTruth = [
	"Fried Apple Pies",
	"Cranberry Apple Pie II",
	"French Apple Cream Pie",
	"Fresh Apple Cake",
	"Apple Pie 2",
	"Fresh Apple Walnut Cake",
	"Apple Blueberry Tarts"
	]

	# Baseline: Matching recipes based on title.
	matchedByTitle =  getRecipesByMatchingTitle(query, len(groundTruth))
	# Matching by similarity
	matchedBySimilarity = getRecipesBySimilarity(query, len(groundTruth))

	# Evaluationg each method:
	print "Baseline Accuracy"
	print calculateAccuracy(matchedByTitle, groundTruth)
	print "Similarity Algorithm accuracy"
	print calculateAccuracy(matchedBySimilarity, groundTruth)

print "One Word Query"
firstExperiment()
print "\n\nTwo words query"
secondExperiment()