"""
	Authors: Victor Trejo
	This script starts a web server for the demo
"""
import BaseHTTPServer
import CGIHTTPServer 

# creating http server instance
baseServer = BaseHTTPServer.HTTPServer
# creating CGI request handler to be able to make
# requests to python scripts
cgiHandler = CGIHTTPServer.CGIHTTPRequestHandler
cgiHandler.cgi_directories = ["/"]
# starting the server, listening at port 12345
baseServer(("", 12345), cgiHandler).serve_forever()